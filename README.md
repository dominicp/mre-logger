mre-logger
==========
Sometimes you just want to write logs to stdout, print error stack traces, and configure levels. That's
what you get here.

I realized I was using the fantastic winston logger for something that could be done in about 60 lines
of code. So, there's one less dependency in my projects.


Usage
-----
I like to get a single logger instance and use it throughout the app.

### logger.js
```js
import getLogger from 'mre-logger';

export default getLogger({ level: 'debug' }, process.env.NODE_ENV);
```

### app.js
```js
import logger from './logger';

logger.info('The %s is falling', 'sky', new Error('The system is down...'));
```


Notes
-----
If the environment is set to `test` this won't output anything because I hate system log messages
polluting my test run results.

This is basically just a fancy wrapper around Node's `util.format()`. Anything you pass in, just
gets passed right on through to that and written to the process's standard out. Simple.
