/**
 * A dead simple logger that only logs to standard out
 */
const { format, formatWithOptions } = require('util');
const chalk = require('chalk');

/**
 * Get a logger object that has .info, .warn, .error, etc. methods for logging messages
 *
 * @param {Object} settings Define the log level and pass formatOpts to util.format
 * @param {String} environment The current environment (read from process.env.NODE_ENV as a fallback)
 *
 * @returns {Object} The logging object to use in your app
 */
const getLogger = (settings = {}, environment = process.env.NODE_ENV) => {

    // Define levels and if we are in a development environment
    const isDev = (environment === 'development');
    const minLevel = settings.level || (environment === 'production' ? 'info' : 'debug');
    const levels = {
        'silly': { i: 0, color: 'magenta' },
        'debug': { i: 1, color: 'blue' },
        'info': { i: 2, color: 'green' },
        'warn': { i: 3, color: 'yellow' },
        'error': { i: 4, color: 'red' }
    };

    // Allow consumers to pass in specific options to util.formatWithOptions
    const formatOpts = settings.formatOpts || {};

    // util.formatWithOptions was added in Node v10, this lets us support older versions
    const utilFormat = (...args) => formatWithOptions
        ? formatWithOptions({ colors: isDev, breakLength: Infinity, depth: 10, ...formatOpts }, ...args)
        : format(...args)

    /**
     * A higher order function to get a logger function that outputs messages at a given log level. The
     * output function just uses util.format() on all of the given arguments.
     *
     * @param {String} level The log level to output the current message as
     */
    const log = level => (...args) => {

        // Don't output in testing or if the level of the message is lower than requested
        if (environment === 'test'
            || (minLevel && levels[minLevel] && levels[level].i < levels[minLevel].i)) {

            return;
        }

        // If in development, colorize the level
        let levelTag = level;

        if (isDev) {

            levelTag = chalk[levels[level].color](levelTag);
        }

        // Sometimes util.format leaves literal \n characters in nested strings.
        // We want actual line breaks, so we have this
        const message = utilFormat(...args).replace(/\s*\\n\s*/g, '\n    ');

        // Write the log
        process.stdout.write(levelTag + ': ' + message + '\n');
    };

    // Return an object that has methods for each defined log level
    return Object.keys(levels).reduce((acc, level) => {

        acc[level] = log(level);
        return acc;

    }, {});
};

// Setup a generic logger using default settings for easy use
const defaultLogger = getLogger();

getLogger.log = defaultLogger.info;

Object.assign(getLogger, defaultLogger);

// Export the get logger function
module.exports = getLogger;
